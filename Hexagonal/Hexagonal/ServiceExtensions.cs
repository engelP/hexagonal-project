﻿using Hexagonal.Features.Course.Application.Services;
using Hexagonal.Features.Course.Application.UseCases;
using Hexagonal.Features.Course.Domain.Ports.In;
using Hexagonal.Features.Course.Domain.Ports.Out;
using Hexagonal.Features.Course.Infrastructure;
using Hexagonal.Features.Login.Application.UseCases;
using Hexagonal.Features.Login.Domain.Ports.In;
using Hexagonal.Features.Role.Application.Services;
using Hexagonal.Features.Role.Application.UseCases;
using Hexagonal.Features.Role.Domain.Ports.In;
using Hexagonal.Features.Role.Domain.Ports.Out;
using Hexagonal.Features.Role.Infrastructure;
using Hexagonal.Features.User.Application.Services;
using Hexagonal.Features.User.Application.UseCases;
using Hexagonal.Features.User.Domain.Ports.In;
using Hexagonal.Features.User.Domain.Ports.Out;
using Hexagonal.Features.User.Infrastructure;
using Hexagonal.Shared.Infrastructure.Context;
using Microsoft.EntityFrameworkCore;

namespace Hexagonal
{
    public static class ServiceExtensions
    {
        public static void AddSeederExtension(this IServiceCollection services)
        {
            ServiceProvider serviceProvider = services.BuildServiceProvider();
            using (AppDbContext? context = serviceProvider.GetService<AppDbContext>())
            {
                try
                {
                    if (context != null)
                    {
                        context.Database.Migrate(); // Asegurar que la base de datos está creada y migrada
                        SeedData.Initialize(context); // Llamar al método de inicialización
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error al inicializar la base de datos: {ex.Message}");
                }
            }
        }

        public static void AddCourseExtension(this IServiceCollection services)
        {
            services.AddScoped<ICourseDbAdapterPort, CourseDbAdapter>();
            services.AddScoped<ICourseServicePort, CourseService>();
            services.AddScoped<IGetCourses, GetCoursesImpl>();
            services.AddScoped<IGetCourseById, GetCourseByIdImpl>();
            services.AddScoped<ICreateCourse, CreateCourseImpl>();
            services.AddScoped<IUpdateCourse, UpdateCourseImpl>();
            services.AddScoped<IDeleteCourse, DeleteCourseImpl>();
        }

        public static void AddUserExtension(this IServiceCollection services)
        {
            services.AddScoped<IUserDbAdapterPort, UserDbAdapter>();
            services.AddScoped<IGetUsers, GetUsersImpl>();
            services.AddScoped<IGetUserById, GetUserByIdImpl>();
            services.AddScoped<IGetUserByEmail, GetUserByEmailImpl>();
            services.AddScoped<ICreateUser, CreateUserImpl>();
            services.AddScoped<IUpdateUser, UpdateUserImpl>();
            services.AddScoped<IDeleteUser, DeleteUserImpl>();
            services.AddScoped<IUserServicePort, UserService>();
            services.AddScoped<IAddRoles, AddRolesImpl>();
            services.AddScoped<IRemoveRoles, RemoveRolesImpl>();
        }

        public static void AddRoleExtension(this IServiceCollection services)
        {
            services.AddScoped<IRoleDbAdapterPort, RoleDbAdapter>();
            services.AddScoped<IGetRoles, GetRolesImpl>();
            services.AddScoped<IGetRoleById, GetRoleByIdImpl>();
            services.AddScoped<ICreateRole, CreateRoleImpl>();
            services.AddScoped<IUpdateRole, UpdateRoleImpl>();
            services.AddScoped<IDeleteRole, DeleteRoleImpl>();
            services.AddScoped<IRoleServicePort, RoleService>();
        }

        public static void AddLoginExtension(this IServiceCollection services)
        {
            services.AddScoped<ILogin, LoginImpl>();
        }
    }
}
