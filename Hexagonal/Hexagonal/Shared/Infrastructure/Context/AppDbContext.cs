﻿using Hexagonal.Features.Course.Infrastructure;
using Hexagonal.Features.Role.Infrastructure;
using Hexagonal.Features.User.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace Hexagonal.Shared.Infrastructure.Context
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        public DbSet<CourseEntity> Courses { get; set; }
        public DbSet<UserEntity> Users { get; set; }
        public DbSet<RoleEntity> Roles { get; set; }
    }
}
