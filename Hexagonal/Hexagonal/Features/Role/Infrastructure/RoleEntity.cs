﻿using Hexagonal.Features.Role.Domain.Models;
using Hexagonal.Features.User.Infrastructure;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hexagonal.Features.Role.Infrastructure
{
    [Table("Role")]
    public class RoleEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [MaxLength(100)]
        public string? Description { get; set; }

        public virtual ICollection<UserEntity> Users { get; set; }

        public RoleEntity FromModel(RoleModel model)
        {
            Id = model.Id;
            Name = model.Name;
            Description = model.Description;
            return this;
        }

        public RoleModel ToModel()
        {
            return new RoleModel
            {
                Id = Id,
                Name = Name,
                Description = Description
            };
        }

        public void UpdatePropsFromModel(RoleModel model)
        {
            Name = model.Name ?? Name;
            Description = model.Description ?? Description;
        }
    }
}
