﻿using Hexagonal.Features.Role.Domain.Ports.In;
using Hexagonal.Features.Role.Domain.Ports.Out;

namespace Hexagonal.Features.Role.Application.UseCases
{
    public class DeleteRoleImpl : IDeleteRole
    {
        private readonly IRoleDbAdapterPort _adapter;

        public DeleteRoleImpl(IRoleDbAdapterPort adapter)
        {
            _adapter = adapter;
        }

        public Task<int> Delete(int id)
        {
            return _adapter.Delete(id);
        }
    }
}
