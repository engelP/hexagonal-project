﻿using Hexagonal.Features.Role.Domain.Models;
using Hexagonal.Features.Role.Domain.Ports.In;
using Hexagonal.Features.Role.Domain.Ports.Out;

namespace Hexagonal.Features.Role.Application.UseCases
{
    public class GetRoleByIdImpl : IGetRoleById
    {
        private readonly IRoleDbAdapterPort _adapter;

        public GetRoleByIdImpl(IRoleDbAdapterPort adapter)
        {
            _adapter = adapter;
        }

        public Task<RoleModel> GetRoleById(int id)
        {
            return _adapter.FindById(id);
        }
    }
}
