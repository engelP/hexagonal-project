﻿using Hexagonal.Features.Role.Domain.Models;
using Hexagonal.Features.Role.Domain.Ports.In;

namespace Hexagonal.Features.Role.Application.Services
{
    public class RoleService : IRoleServicePort
    {
        private readonly IGetRoles _getRoles;
        private readonly IGetRoleById _getRoleById;
        private readonly ICreateRole _createRole;
        private readonly IUpdateRole _updateRole;
        private readonly IDeleteRole _deleteRole;

        public RoleService(IGetRoles getRoles, IGetRoleById getRoleById, ICreateRole createRole, IUpdateRole updateRole, IDeleteRole deleteRole)
        {
            _getRoles = getRoles;
            _getRoleById = getRoleById;
            _createRole = createRole;
            _updateRole = updateRole;
            _deleteRole = deleteRole;
        }

        public Task<List<RoleModel>> GetRoles()
        {
            return _getRoles.GetRoles();
        }

        public Task<RoleModel> GetRoleById(int id)
        {
            return _getRoleById.GetRoleById(id);
        }

        public Task<RoleModel> Create(RoleModel role)
        {
            return _createRole.Create(role);
        }

        public Task<RoleModel> Update(int id, RoleModel role)
        {
            return _updateRole.Update(id, role);
        }

        public Task<int> Delete(int id)
        {
            return _deleteRole.Delete(id);
        }
    }
}
