﻿using Hexagonal.Features.Role.Domain.Models;

namespace Hexagonal.Features.Role.Domain.Ports.In
{
    public interface IGetRoleById
    {
        Task<RoleModel> GetRoleById(int id);
    }
}
