﻿using Hexagonal.Features.Role.Domain.Models;

namespace Hexagonal.Features.Role.Domain.Ports.In
{
    public interface IUpdateRole
    {
        Task<RoleModel> Update(int id, RoleModel role);
    }
}
