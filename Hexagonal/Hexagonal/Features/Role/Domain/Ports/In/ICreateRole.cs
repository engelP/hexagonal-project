﻿using Hexagonal.Features.Role.Domain.Models;

namespace Hexagonal.Features.Role.Domain.Ports.In
{
    public interface ICreateRole
    {
        Task<RoleModel> Create(RoleModel role);
    }
}
