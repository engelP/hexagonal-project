﻿using Hexagonal.Features.Role.Domain.Models;
using Hexagonal.Shared.Domain.Ports.Out;

namespace Hexagonal.Features.Role.Domain.Ports.Out
{
    public interface IRoleDbAdapterPort : IDbCrudAdapter<RoleModel>
    {
    }
}
