﻿using Hexagonal.Features.User.Domain.Models;
using System.ComponentModel.DataAnnotations;

namespace Hexagonal.Features.Role.Domain.Models
{
    public class RoleModel
    {
        public int Id { get; set; }

        [MaxLength(100)]
        public string Name { get; set; }

        [MaxLength(100)]
        public string? Description { get; set; }

        public virtual ICollection<UserModel> Users { get; set; }
    }
}
