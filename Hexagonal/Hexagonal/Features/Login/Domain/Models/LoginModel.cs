﻿using System.ComponentModel.DataAnnotations;

namespace Hexagonal.Features.Login.Domain.Models
{
    public class LoginModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
