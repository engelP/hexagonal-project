﻿namespace Hexagonal.Features.Login.Domain
{
    public class CredentialsException : Exception
    {
        public CredentialsException() : base("Invalid credentials")
        {
        }
    }
}
