﻿using Dapper;
using Hexagonal.Features.Course.Domain.Models;
using Hexagonal.Features.Course.Domain.Ports.Out;
using Hexagonal.Shared.Infrastructure.Context;
using Hexagonal.Shared.Infrastructure.DapperContext;
using Microsoft.EntityFrameworkCore;

namespace Hexagonal.Features.Course.Infrastructure
{
    public class CourseDbAdapter : ICourseDbAdapterPort
    {
        private readonly AppDbContext _db;
        private readonly DapperContext _dapperContext;

        public CourseDbAdapter(AppDbContext db, DapperContext dapperContext)
        {
            _db = db;
            _dapperContext = dapperContext;
        }

        public async Task<List<CourseModel>> FindAll()
        {
            List<CourseModel> courses = await _db.Courses
                .Select(x => x.ToModel())
                .ToListAsync();

            return courses;
        }

        public async Task<CourseModel> FindById(int id)
        {
            CourseEntity? courseFound = await _db.Courses.FirstOrDefaultAsync(i => i.Id == id);

            return courseFound == null
                ? throw new Exception($"Course with id {id} not found")
                : courseFound.ToModel();
        }

        public async Task<CourseModel> Create(CourseModel model)
        {
            try
            {
                CourseEntity entity = new CourseEntity().FromModel(model);
                _db.Courses.Add(entity);
                await _db.SaveChangesAsync();
                return entity.ToModel();
            }
            catch (Exception e)
            {
                throw new Exception("Error creating course", e);
            }
        }

        public async Task<CourseModel> Update(int id, CourseModel model)
        {
            CourseEntity? entity = await _db.Courses.FirstOrDefaultAsync(x => x.Id == id);

            if (entity == null)
            {
                throw new Exception("Course not found!");
            }

            try
            {
                entity.UpdatePropsFromModel(model);

                _db.Entry(entity).State = EntityState.Modified;
                await _db.SaveChangesAsync();

                return entity.ToModel();
            }
            catch (Exception e)
            {
                throw new Exception("Error updating course", e);
            }
        }

        public async Task<int> Delete(int id)
        {
            CourseEntity? courseEntity = await _db.Courses.FindAsync(id);

            if (courseEntity == null)
            {
                throw new Exception("Course not found!");
            }

            try
            {
                _db.Courses.Remove(courseEntity);

                await _db.SaveChangesAsync();

                return id;
            }
            catch (Exception e)
            {
                throw new Exception("Error deleting course", e);
            }
        }
    }
}
