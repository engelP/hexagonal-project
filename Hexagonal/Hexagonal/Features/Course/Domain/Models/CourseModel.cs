﻿using Hexagonal.Features.User.Domain.Models;
using System.ComponentModel.DataAnnotations;

namespace Hexagonal.Features.Course.Domain.Models
{
    public class CourseModel
    {
        public int Id { get; set; }

        [MaxLength(100)]
        public string Name { get; set; }

        [MaxLength(500)]
        public string? Description { get; set; }

        public int InstructorId { get; set; }

        public virtual UserModel Instructor { get; set; }

        public virtual ICollection<UserModel> Students { get; set; }
    }
}
