﻿namespace Hexagonal.Features.Course.Domain.Ports.In
{
    public interface IDeleteCourse
    {
        Task<int> Delete(int id);
    }
}
