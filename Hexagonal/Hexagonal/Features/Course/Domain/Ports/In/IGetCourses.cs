﻿using Hexagonal.Features.Course.Domain.Models;

namespace Hexagonal.Features.Course.Domain.Ports.In
{
    public interface IGetCourses
    {
        Task<List<CourseModel>> GetCourses();
    }
}
