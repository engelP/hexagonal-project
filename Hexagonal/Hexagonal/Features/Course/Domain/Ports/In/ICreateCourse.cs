﻿using Hexagonal.Features.Course.Domain.Models;

namespace Hexagonal.Features.Course.Domain.Ports.In
{
    public interface ICreateCourse
    {
        Task<CourseModel> Create(CourseModel course);
    }
}
