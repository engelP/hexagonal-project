﻿using Hexagonal.Features.Course.Domain.Models;

namespace Hexagonal.Features.Course.Domain.Ports.In
{
    public interface IGetCourseById
    {
        Task<CourseModel> GetCourseById(int id);
    }
}
