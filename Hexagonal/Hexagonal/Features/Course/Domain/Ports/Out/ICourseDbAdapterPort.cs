﻿using Hexagonal.Features.Course.Domain.Models;
using Hexagonal.Shared.Domain.Ports.Out;

namespace Hexagonal.Features.Course.Domain.Ports.Out
{
    public interface ICourseDbAdapterPort : IDbCrudAdapter<CourseModel>
    {
    }
}
