﻿using Hexagonal.Features.Course.Domain.Models;
using Hexagonal.Features.Course.Domain.Ports.In;
using Hexagonal.Features.Course.Domain.Ports.Out;

namespace Hexagonal.Features.Course.Application.UseCases
{
    public class GetCourseByIdImpl : IGetCourseById
    {
        private readonly ICourseDbAdapterPort _adapter;

        public GetCourseByIdImpl(ICourseDbAdapterPort adapter)
        {
            _adapter = adapter;
        }

        public Task<CourseModel> GetCourseById(int id)
        {
            return _adapter.FindById(id);
        }
    }
}
