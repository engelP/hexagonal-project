﻿using Hexagonal.Features.Course.Domain.Ports.In;
using Hexagonal.Features.Course.Domain.Ports.Out;

namespace Hexagonal.Features.Course.Application.UseCases
{
    public class DeleteCourseImpl : IDeleteCourse
    {
        private readonly ICourseDbAdapterPort _adapter;

        public DeleteCourseImpl(ICourseDbAdapterPort adapter)
        {
            _adapter = adapter;
        }

        public Task<int> Delete(int id)
        {
            return _adapter.Delete(id);
        }
    }
}
