﻿using Hexagonal.Features.Course.Domain.Models;
using Hexagonal.Features.Course.Domain.Ports.In;
using Hexagonal.Features.Course.Domain.Ports.Out;

namespace Hexagonal.Features.Course.Application.UseCases
{
    public class UpdateCourseImpl : IUpdateCourse
    {
        private readonly ICourseDbAdapterPort _adapter;

        public UpdateCourseImpl(ICourseDbAdapterPort adapter)
        {
            _adapter = adapter;
        }

        public Task<CourseModel> Update(int id, CourseModel course)
        {
            return _adapter.Update(id, course);
        }
    }
}
