﻿using Hexagonal.Features.Course.Domain.Models;
using Hexagonal.Features.Course.Domain.Ports.In;

namespace Hexagonal.Features.Course.Application.Services
{
    public class CourseService : ICourseServicePort
    {
        private readonly IGetCourses _getCourses;
        private readonly IGetCourseById _getCourseById;
        private readonly ICreateCourse _createCourse;
        private readonly IUpdateCourse _updateCourse;
        private readonly IDeleteCourse _deleteCourse;

        public CourseService(IGetCourses getCourses, IGetCourseById getCourseById, ICreateCourse createCourse, IUpdateCourse updateCourse, IDeleteCourse deleteCourse)
        {
            _getCourses = getCourses;
            _getCourseById = getCourseById;
            _createCourse = createCourse;
            _updateCourse = updateCourse;
            _deleteCourse = deleteCourse;
        }

        public Task<List<CourseModel>> GetCourses()
        {
            return _getCourses.GetCourses();
        }

        public Task<CourseModel> GetCourseById(int id)
        {
            return _getCourseById.GetCourseById(id);
        }

        public Task<CourseModel> Create(CourseModel course)
        {
            return _createCourse.Create(course);
        }

        public Task<CourseModel> Update(int id, CourseModel course)
        {
            return _updateCourse.Update(id, course);
        }

        public Task<int> Delete(int id)
        {
            return _deleteCourse.Delete(id);
        }
    }
}
