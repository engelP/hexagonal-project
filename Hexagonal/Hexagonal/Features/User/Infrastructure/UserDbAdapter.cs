﻿using Hexagonal.Features.Role.Infrastructure;
using Hexagonal.Features.User.Domain.Models;
using Hexagonal.Features.User.Domain.Ports.Out;
using Hexagonal.Shared.Infrastructure.Context;
using Microsoft.EntityFrameworkCore;

namespace Hexagonal.Features.User.Infrastructure
{
    public class UserDbAdapter : IUserDbAdapterPort
    {
        private readonly AppDbContext _db;

        public UserDbAdapter(AppDbContext context)
        {
            _db = context;
        }

        public async Task<List<UserModel>> FindAll()
        {
            List<UserModel> users = await _db.Users
                .Select(x => x.ToModel())
                .ToListAsync();

            return users;
        }

        public async Task<UserModel?> FindById(int id)
        {
            UserEntity? userFound = await _db.Users
                .FirstOrDefaultAsync(i => i.Id == id);

            var userModel = userFound?.ToModel();

            if (userModel != null && userFound != null)
            {
                userModel.Roles = userFound.LoadRoles();
            }

            return userModel;
        }

        public async Task<UserModel> Create(UserModel user)
        {
            try
            {
                UserEntity entity = new UserEntity().FromModel(user);
                _db.Users.Add(entity);
                await _db.SaveChangesAsync();
                return entity.ToModel();
            }
            catch (Exception e)
            {
                throw new Exception("Error creating user", e);
            }
        }

        public async Task<UserModel> Update(int id, UserModel user)
        {
            UserEntity? entity = await _db.Users.FirstOrDefaultAsync(x => x.Id == id);

            if (entity == null)
            {
                throw new Exception("User not found!");
            }

            try
            {
                entity.UpdatePropsFromModel(user);

                _db.Entry(entity).State = EntityState.Modified;
                await _db.SaveChangesAsync();

                return entity.ToModel();
            }
            catch (Exception e)
            {
                throw new Exception("Error updating user", e);
            }
        }

        public async Task<int> Delete(int id)
        {
            UserEntity? userEntity = await _db.Users.FindAsync(id);

            if (userEntity == null)
            {
                throw new Exception("User not found!");
            }

            try
            {
                _db.Users.Remove(userEntity);

                await _db.SaveChangesAsync();

                return id;
            }
            catch (Exception e)
            {
                throw new Exception("Error deleting user", e);
            }
        }

        public async Task<UserModel> AddRoles(int userId, List<int> roleIdList)
        {
            UserEntity? userEntity = await _db.Users
                .FirstOrDefaultAsync(x => x.Id == userId);

            if (userEntity == null)
            {
                throw new Exception("User not found!");
            }

            try
            {
                var existingRoles = userEntity.Roles.ToList();
                List<RoleEntity> rolesFound = await _db.Roles
                    .Where(x => roleIdList.Contains(x.Id))
                    .ToListAsync();

                if (!rolesFound.Any())
                {
                    throw new Exception("Roles not found");
                }

                existingRoles.AddRange(rolesFound);

                userEntity.Roles = existingRoles;

                await _db.SaveChangesAsync();

                var userModel = userEntity.ToModel();

                userModel.Roles = userEntity.LoadRoles();

                return userModel;
            }
            catch (Exception e)
            {
                throw new Exception("Error adding roles to user", e);
            }
        }

        public async Task<UserModel> RemoveRoles(int userId, List<int> roleIdList)
        {
            UserEntity? userEntity = await _db.Users
                .FirstOrDefaultAsync(x => x.Id == userId);

            if (userEntity == null)
            {
                throw new Exception("User not found!");
            }

            try
            {
                var existingRoles = userEntity.Roles.ToList();
                List<RoleEntity> rolesFound = await _db.Roles
                    .Where(x => roleIdList.Contains(x.Id))
                    .ToListAsync();

                if (!rolesFound.Any())
                {
                    throw new Exception("Roles not found");
                }

                existingRoles = existingRoles
                    .Where(x => !roleIdList.Contains(x.Id))
                    .ToList();

                userEntity.Roles = existingRoles;

                await _db.SaveChangesAsync();

                var userModel = userEntity.ToModel();

                userModel.Roles = userEntity.LoadRoles();

                return userModel;
            }
            catch (Exception e)
            {
                throw new Exception("Error removing roles from user", e);
            }
        }

        public async Task<UserModel?> FindByEmail(string email)
        {
            UserEntity? userFound = await _db.Users
                .FirstOrDefaultAsync(i => i.Email == email);

            if (userFound == null) return null;

            var userModel = userFound.ToModel();

            userModel.Roles = userFound.LoadRoles();

            return userModel;
        }
    }
}
