﻿using Hexagonal.Features.Course.Domain.Models;
using Hexagonal.Features.Role.Domain.Models;
using System.ComponentModel.DataAnnotations;

namespace Hexagonal.Features.User.Domain.Models
{
    public class UserModel
    {
        public int Id { get; set; }

        [MaxLength(100)]
        public string Name { get; set; }

        [MaxLength(100)]
        public string? LastName { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        public string Password { get; set; }

        public virtual ICollection<CourseModel> IntructorCourses { get; set; }

        public virtual ICollection<CourseModel> StudentCourses { get; set; }

        public virtual ICollection<RoleModel> Roles { get; set; }
    }
}
