﻿using Hexagonal.Features.User.Domain.Models;

namespace Hexagonal.Features.User.Domain.Ports.In
{
    public interface IAddRoles
    {
        Task<UserModel> AddRoles(int userId, List<int> roleIdList);
    }
}
