﻿using Hexagonal.Features.User.Domain.Models;

namespace Hexagonal.Features.User.Domain.Ports.In
{
    public interface IRemoveRoles
    {
        Task<UserModel> RemoveRoles(int userId, List<int> roleIdList);
    }
}
