﻿using Hexagonal.Features.User.Domain;
using Hexagonal.Features.User.Domain.Models;
using Hexagonal.Features.User.Domain.Ports.In;
using Hexagonal.Features.User.Domain.Ports.Out;

namespace Hexagonal.Features.User.Application.UseCases
{
    public class AddRolesImpl : IAddRoles
    {
        private readonly IUserDbAdapterPort _adapter;
        private readonly IGetUserById _getUserById;

        public AddRolesImpl(IUserDbAdapterPort adapter, IGetUserById getUserById)
        {
            _adapter = adapter;
            _getUserById = getUserById;
        }

        public async Task<UserModel> AddRoles(int userId, List<int> roleIdList)
        {
            var user = await _getUserById.GetUserById(userId);
            if (user == null)
            {
                throw new UserNotFoundException(userId);
            }

            return await _adapter.AddRoles(userId, roleIdList);
        }
    }
}
