﻿using Hexagonal.Features.User.Domain.Models;
using Hexagonal.Features.User.Domain.Ports.In;
using Hexagonal.Features.User.Domain.Ports.Out;
using Hexagonal.Features.User.Domain;

namespace Hexagonal.Features.User.Application.UseCases
{
    public class RemoveRolesImpl : IRemoveRoles
    {
        private readonly IUserDbAdapterPort _adapter;
        private readonly IGetUserById _getUserById;

        public RemoveRolesImpl(IUserDbAdapterPort adapter, IGetUserById getUserById)
        {
            _adapter = adapter;
            _getUserById = getUserById;
        }

        public async Task<UserModel> RemoveRoles(int userId, List<int> roleIdList)
        {
            var user = await _getUserById.GetUserById(userId);
            if (user == null)
            {
                throw new UserNotFoundException(userId);
            }

            return await _adapter.RemoveRoles(userId, roleIdList);
        }
    }
}
